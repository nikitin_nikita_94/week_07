package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

import java.util.Objects;

public class SymbolPriceServiceImpl implements SymbolPriceService {

    /**
     * URI сервиса.
     */
    private final String uri = "https://api.binance.com/api/v3/ticker/price";

    /**
     * Логгер.
     */
    public static final Logger LOGGER =
            LoggerFactory.getLogger(SymbolPriceServiceImpl.class);

    /**
     * Шаблонизированный класс для работы
     * с REST.
     */
    private static final RestTemplate REST_TEMPLATE = new RestTemplate();

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные
     * будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName String
     * @return Symbol
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        ResponseEntity<SymbolImpl> forEntity =
                REST_TEMPLATE.getForEntity(uri
                                + "?symbol="
                                + Objects.requireNonNull(symbolName),
                        SymbolImpl.class);

        if (forEntity.getStatusCode() != HttpStatus.OK) {
            LOGGER.error("Не получили код ответа: {}",
                    forEntity.getStatusCodeValue());
        }

        Symbol symbol = forEntity.getBody();
        LOGGER.debug("Получили ответ для {}: {}", symbolName, symbol);
        return symbol;
    }
}
