package ru.edu.model;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

public class SymbolImpl implements Symbol {

    /**
     * Название криптовалюты.
     */
    private String symbol;

    /**
     * Цена криптовалюты.
     */
    private BigDecimal price;

    /**
     * Время получения названия и
     * цены криптовалюты.
     */
    private Instant timeStamp = Instant.now();

    /**
     * Конструктор без аргументов.
     */
    public SymbolImpl() {
    }

    /**
     * Геттер.
     * @return String
     */
    @Override
    public String getSymbol() {
        return symbol;
    }

    /**
     * Сеттер.
     *
     * @param value
     */
    public void setSymbol(final String value) {
        symbol = Objects.requireNonNull(value);
    }

    /**
     * Геттер.
     * @return BigDecimal
     */
    @Override
    public BigDecimal getPrice() {
        return price;
    }

    /**
     * Сеттер.
     * @param value
     */
    public void setPrice(final BigDecimal value) {
        price = Objects.requireNonNull(value);
    }

    /**
     * Время получения данных.
     *
     * @return Instant
     */
    @Override
    public Instant getTimeStamp() {
        return timeStamp;
    }

    /**
     * Сеттер.
     * @param time
     */
    public void setTimeStamp(final Instant time) {
        timeStamp = Objects.requireNonNull(time);
    }

    /**
     * Вывод всех полей.
     * @return String
     */
    @Override
    public String toString() {
        return "SymbolImpl{"
                + "symbol='" + symbol
                + '\'' + ", price=" + price
                + ", timeStamp=" + timeStamp
                + '}';
    }
}
