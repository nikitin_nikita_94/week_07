package ru.edu;

import ru.edu.model.Symbol;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;

public class CashedSymbolPriseService implements SymbolPriceService {

    /**
     * Актуальное нахождение криптовалюты в кеше.
     */
    public static final int SECONDS_OF_CASH = 5;

    /**
     * Сервис для делегирования
     * по получению данных из внешнего источника.
     */
    private final SymbolPriceService delegateService;

    /**
     * Внутренний КЕШ.
     */
    private final Map<String, Symbol> cashedMap = new HashMap<>();

    /**
     * КОнструктор.
     *
     * @param service SymbolPriceService
     */
    public CashedSymbolPriseService(final SymbolPriceService service) {
        delegateService = service;
    }

    /**
     * Сервис по получению данных из внешнего источника.
     * Должен иметь внутренний кэш, с помощью которого данные
     * будут обновляться не чаще, чем раз в 10 секунд.
     *
     * @param symbolName String
     * @return Symbol
     */
    @Override
    public Symbol getPrice(final String symbolName) {
        synchronized (symbolName) {
            if (!cashedMap.containsKey(symbolName)
                    || Instant.now().minus(SECONDS_OF_CASH,
                    ChronoUnit.SECONDS).isAfter(cashedMap.get(symbolName)
                    .getTimeStamp())) {
                cashedMap.put(symbolName, delegateService.getPrice(symbolName));
            }
        }
        return cashedMap.get(symbolName);
    }
}
