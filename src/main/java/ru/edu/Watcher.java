package ru.edu;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import ru.edu.model.Symbol;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;

public class Watcher implements Runnable {

    /**
     * Логгер.
     */
    public static final Logger LOGGER = LoggerFactory
            .getLogger(Watcher.class);

    /**
     * Название отслеживаемой криптовалюты.
     */
    private final String symbolToWatch;

    /**
     * Сервис по получению данных из внешнего источника.
     */
    private final SymbolPriceService service;

    /**
     * Хранит предыдущее значение обьекта.
     */
    private Symbol lastDate;

    /**
     * Конструктор.
     *
     * @param symbol
     * @param servicePrise
     */
    public Watcher(final String symbol, final SymbolPriceService servicePrise) {
        this.symbolToWatch = symbol;
        this.service = servicePrise;
    }

    /**
     * When an object implementing interface {@code Runnable} is used
     * to create a thread, starting the thread causes the object's
     * {@code run} method to be called in that separately executing
     * thread.
     * <p>
     * The general contract of the method {@code run} is that it may
     * take any action whatsoever.
     *
     * @see Thread#run()
     */
    @Override
    public void run() {
        Symbol symbol = service.getPrice(symbolToWatch);
        LOGGER.info("Получили ответ: {}, ранее: {}", symbol, lastDate);
        cryptoWriter(symbol);
        lastDate = symbol;
    }

    private void cryptoWriter(final Symbol symbol) {
        try (FileWriter writer = new FileWriter(
                "src/test/java/ru/edu/pricehistory/crypto.txt", true)) {

            String difference;
            if (lastDate == null) {
                difference = "0";
            } else {
                difference = String
                        .valueOf(lastDate.getPrice()
                        .subtract(symbol.getPrice()));
            }

            writer.write(LocalDate.now()
                    + " | " + symbol.getPrice()
                    + " "
                    + difference + "\n");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        }
    }
}
