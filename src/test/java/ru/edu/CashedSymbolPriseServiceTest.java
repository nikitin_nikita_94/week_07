package ru.edu;

import org.junit.Test;
import org.mockito.Spy;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

import static org.junit.Assert.*;
import static ru.edu.SymbolPriceServiceTest.SYMBOL_NAME;

public class CashedSymbolPriseServiceTest {

    @Spy
    SymbolPriceService service = new SymbolPriceServiceImpl();

    @Spy
    SymbolPriceService priceService = new CashedSymbolPriseService(service);


    @Test
    public void getPrice() throws InterruptedException {
        Symbol expected = priceService.getPrice(SYMBOL_NAME);
        Symbol actual = priceService.getPrice(SYMBOL_NAME);
        assertEquals(expected.getPrice(),actual.getPrice());

        Thread.sleep(12_000L);

        Symbol newActual = priceService.getPrice(SYMBOL_NAME);
        assertTrue(actual.getTimeStamp().isBefore(newActual.getTimeStamp()));
    }
}