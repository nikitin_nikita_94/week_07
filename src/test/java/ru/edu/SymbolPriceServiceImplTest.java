package ru.edu;

import org.junit.Test;
import ru.edu.model.Symbol;

import static org.junit.Assert.*;

public class SymbolPriceServiceImplTest {

    @Test
    public void getPrice() {
        SymbolPriceService service = new SymbolPriceServiceImpl();
        Symbol price = service.getPrice("BTCUSDT");
        assertNotNull(price);
    }

    @Test(expected = NullPointerException.class)
    public void getPriceNullTest() {
        SymbolPriceService service = new SymbolPriceServiceImpl();
        Symbol price = service.getPrice(null);
    }
}