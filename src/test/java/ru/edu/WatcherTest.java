package ru.edu;

import org.junit.Test;
import org.mockito.Spy;

public class WatcherTest {

    @Spy
    CashedSymbolPriseService service = new CashedSymbolPriseService(new SymbolPriceServiceImpl());

    @Spy
    Watcher watcher;

    private final static String SYMBOL = "BTCUSDT";


    @Test
    public void run() throws InterruptedException {
        watcher = new Watcher(SYMBOL,service);
        for (int i = 0; i < 5; i++) {
            watcher.run();
            Thread.sleep(3_000L);
        }
    }
}