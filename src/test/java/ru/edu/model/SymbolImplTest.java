package ru.edu.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Spy;

import java.math.BigDecimal;
import java.time.Instant;

import static org.junit.Assert.*;

public class SymbolImplTest {

    @Spy
    SymbolImpl symbol = new SymbolImpl();

    @Before
    public void setUp() {
        symbol.setSymbol("BTCRUB");
        symbol.setPrice(new BigDecimal("4.01253"));
        symbol.setTimeStamp(Instant.now());
    }

    @Test
    public void getSymbol() {
        assertEquals("BTCRUB",symbol.getSymbol());
    }

    @Test
    public void setSymbol() {
        symbol.setSymbol("BTCUSDT");
        assertEquals("BTCUSDT",symbol.getSymbol());
    }

    @Test
    public void getPrice() {
        assertEquals(new BigDecimal("4.01253"),symbol.getPrice());
    }

    @Test
    public void setPrice() {
        symbol.setPrice(new BigDecimal("6.01253"));
        assertEquals(new BigDecimal("6.01253"),symbol.getPrice());
    }

    @Test
    public void getTimeStamp() {
        assertEquals(Instant.now(),symbol.getTimeStamp());
    }

    @Test
    public void testToString() {
        String expected = symbol.toString();
        assertEquals(expected,symbol.toString());
    }
}