package ru.edu;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.client.RestTemplate;
import ru.edu.model.Symbol;
import ru.edu.model.SymbolImpl;

import java.time.Instant;

import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class SymbolPriceServiceTest {
    public static final Logger LOGGER = LoggerFactory.getLogger(SymbolPriceServiceTest.class);

    public static final String SYMBOL_NAME = "BTCRUB";

    private SymbolPriceService service = mock(SymbolPriceServiceImpl.class);;
    private SymbolPriceService cashService = mock(CashedSymbolPriseService.class);

    private Symbol mockSymbol = mock(SymbolImpl.class);

    @Test
    public void getPrice() throws InterruptedException {

        /**
         * примерный тест проверки работы кэша, делаем 2 вызова и смотрим что данные были получены в одно время
         * после ожидания таймаута жизни данных в кэше делаем повторный запрос и проверяем, что данные имеют метку времени,
         * полученную после сброса.
         */

        when(service.getPrice(SYMBOL_NAME)).thenReturn(mockSymbol);
        when(mockSymbol.getTimeStamp()).thenReturn(Instant.now());

        Symbol price = service.getPrice(SYMBOL_NAME);
        Symbol priceAgain = service.getPrice(SYMBOL_NAME);
        assertEquals(price.getTimeStamp(), priceAgain.getTimeStamp());

        long millis = 11_000L;
        LOGGER.info("Ждем сброса кэша {}",millis);
        Thread.sleep(millis);

        Symbol priceNew = service.getPrice(SYMBOL_NAME);
        assertFalse(price.getTimeStamp().isBefore(priceNew.getTimeStamp()));
    }
}